#!/usr/bin/env python
"""Tests for `an_ant_world` package."""


import unittest
import re
#from click.testing import CliRunner

#from an_ant_world import cli
from an_ant_world.environment import CartesianEnvironment
from an_ant_world.agent import RandomWalker, SmellyWalker
from an_ant_world.simulation import GridSimulation


class TestAnAntWorld(unittest.TestCase):
    """Tests for `an_ant_world` package."""

    def test_smelly_walker(self):
        """
        Test SmellyWalker.
        """
        n_agents = 10
        n_timesteps = 4
        agents = {SmellyWalker() for iid in range(n_agents)}
        positions = [(5, 5) for ag in agents]
        sim = GridSimulation(agents_set=agents,
                             total_timesteps=n_timesteps,
                             environment=CartesianEnvironment(sizes=(10, 10)),
                             agents_positions=positions)
        self.assertEqual(sim.run_simulation(outfile_positions='outpos.dat'),
                         n_timesteps)
        r_dg = re.compile('\\d+ \\d+')
        r_nl = re.compile('\n')
        with open('outpos.dat', 'r') as f_pos:
            for l_i in f_pos.readlines():
                is_positions = r_dg.match(l_i.strip())
                is_blank = r_nl.match(l_i)
                self.assertIsNotNone(is_positions or is_blank)
        self.assertEqual(sim.get_environment().get_resources()['pheromone']
                         .sum(),
                         n_agents*n_timesteps)

    def test_random_walker(self):
        """Test RandomWalker."""
        n_agents = 2
        n_timesteps = 10
        agents = {RandomWalker() for iid in range(n_agents)}
        positions = [(5, 5) for ag in agents]
        sim = GridSimulation(
            agents_set=agents,
            total_timesteps=n_timesteps,
            environment=CartesianEnvironment(sizes=(10, 10)),
            agents_positions=positions)
        self.assertEqual(sim.run_simulation(), n_timesteps)

    # def test_command_line_interface(self):
    #     """Test the CLI."""
    #     runner = CliRunner()
    #     result = runner.invoke(cli.main)
    #     assert result.exit_code == 0
    #     assert 'an_ant_world.cli.main' in result.output
    #     help_result = runner.invoke(cli.main, ['--help'])
    #     assert help_result.exit_code == 0
    #     assert '--help  Show this message and exit.' in help_result.output
