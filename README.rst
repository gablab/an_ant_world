============
an_ant_world
============


.. image:: https://img.shields.io/pypi/v/an_ant_world.svg
        :target: https://pypi.python.org/pypi/an_ant_world

.. image:: https://img.shields.io/travis/mujina93/an_ant_world.svg
        :target: https://travis-ci.com/mujina93/an_ant_world

.. image:: https://readthedocs.org/projects/an-ant-world/badge/?version=latest
        :target: https://an-ant-world.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Agent based simulations


* Free software: GNU General Public License v3
* Documentation: https://an-ant-world.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
