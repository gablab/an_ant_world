"""
TODO docstring`
"""
from abc import ABC, abstractmethod
import numpy as np

class Environment(ABC):
    """abstract class representing the environment in which `Agent`s are moving
    """


    def get_resources(self):
        """
        Simple getter.
        """

    @abstractmethod
    def add_resources(self, resources):
        """TODO docstring
        """

    @abstractmethod
    def apply_actions(self, agent, messages):
        """
        The actions made by agents are in the dictionary `messages`.
        """

    @abstractmethod
    def get_view(self, local_info):
        """TODO docstring
        """



class CartesianEnvironment(Environment):
    """
    Environment using Cartesian coordinates
    """

    def __init__(self, sizes):
        """
        `sizes` is a tuple with the size for each Cartesian dimension
        """
        super().__init__()
        self._sizes = sizes
        self._dimensions = len(sizes)
        self._space = np.zeros(np.multiply(*self._sizes)).reshape(*self._sizes)
        self._agents_positions = {}
        self._resources = {}

    def get_dimensions(self):
        """
        Simple getter.
        """
        return self._dimensions

    def get_sizes(self):
        """
        Simple getter.
        """
        return self._sizes

    def add_agents(self, agents_set, positions):
        """
        Process the agents in order to dinamicaly adapt the environment.
        """
        for agent, position in zip(agents_set, positions):
            self._agents_positions[agent.get_index()] = [int(p) for p in position]
            self.add_resources(agent.get_requirements())

    def get_position(self, agent):
        """
        Return position of the given agent.
        """
        return self._agents_positions[agent.get_index()]

    def apply_actions(self, agent, messages):
        """
        The actions made by agents are in the dictionary `messages`.
        """
        for msg in messages:
            msg_type = msg['type']
            if msg_type == 'move': # v = relative position
                self._agents_positions[agent.get_index()] += msg['where']
            elif msg_type == 'increase':
                resource_to_incr = msg['what']
                relative_pos = msg['relative_position']
                incr_value = msg['how_much']
                ag_pos = self._agents_positions[agent.get_index()]
                # select the position corresponding to the position
                #  of the agent, translated according to the message:
                #  relative_pos is a space-dimensional vector.
                indices = tuple([
                    slice(ag_pos[dim]+int(relative_pos[dim]),
                          ag_pos[dim]+int(relative_pos[dim])+1)
                    for dim in range(len(self._sizes))
                    ])
                self._resources[resource_to_incr][indices] += incr_value


    def get_view(self, local_info):
        """
        Returns the view of the environment for a given agent (e.g. the food in its surroundings)
        """
        idx = local_info['index']
        rng = local_info['sight']
        pos = self._agents_positions[idx]
        indices = tuple([
            slice(pos[dim]-rng, pos[dim]+rng+1)
            for dim in range(len(self._sizes))
                ])
        return_dict = {rname: rarr[indices] for rname, rarr in self._resources.items()}
        return_dict.update({'space': self._space[indices]})
        return return_dict


    def add_resources(self, resources):
        """
        adds to the environment the containers corresponding to the
        resources (e.g. food, pheromone)
        """
        for res in resources:
            rname = res['name']
            if rname not in self._resources:
                sizes = list(self._sizes)
                sizes.append(res['dimension'])
                # setattr not good because need to iterate over _resources
                self._resources[rname] = np.zeros(shape=sizes)

    def get_resources(self):
        """
        Get a snapshot of the resources array.
        """
        return self._resources
