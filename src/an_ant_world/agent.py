"""
The module of agents.
"""
from abc import ABC, abstractmethod
import numpy as np

class Agent(ABC):
    """
    abstract class representing a generic agent
    """

    _index = 0

    def __init__(self):
        Agent._index += 1
        self._index = Agent._index

    @abstractmethod
    def act(self, view):
        """TODO docstring
        """

    @abstractmethod
    def get_local_info(self):
        """TODO docstring
        """

    @abstractmethod
    def get_requirements(self):
        """
        Return the requirements of the agent for the Environment.
        """
        return {}

    def get_index(self):
        """
        Simple getter for _index
        """
        return self._index


class RandomWalker(Agent):
    """
    Agent that only walks randomly
    """
    def __init__(self, sight=2):
        super().__init__()
        self._sight = sight
        self._requirements = []

    def act(self, view):
        messages = []
        messages.append(step_random(view))
        return messages


    def get_local_info(self):
        return {
            'index': self._index,
            'sight': self._sight
                }

    def get_requirements(self):
        return self._requirements



class SmellyWalker(RandomWalker):
    """
    Random walker releasing pheromone where it passes.
    """
    def __init__(self, sight=2):
        super().__init__(sight)
        self._requirements = [{
            'name': 'pheromone',
            'dimension': 1
            }]

    def act(self, view):
        messages = []
        messages.append(step_random(view))
        messages.append(drop_pheromone(view))
        return messages

    def get_requirements(self):
        return self._requirements

def step_random(view):
    """
    Returns a message which requests to move the agent:
    - 1/9 probability of staying still;
    - 1/8 probability of taking a given direction.
    """
    return {'type': 'move',
            'where': np.random.randint(low=-1, high=2, size=len(view['space'].shape), dtype=int)}

def drop_pheromone(view):
    """
    Returns a messages which requests that pheromone is increased where the walker steps.
    """
    return {'type': 'increase',
            'what': 'pheromone',
            'relative_position': np.zeros(len(view['space'].shape)),
            'how_much': 1}
    #view['pheromone'][self._sight, self._sight] += 1
