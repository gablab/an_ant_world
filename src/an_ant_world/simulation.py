"""
TODO docstring
"""
from abc import ABC, abstractmethod

class Simulation(ABC):
    """
    abstract class for instancing a simulation
    """

    @abstractmethod
    def run_simulation(self, outfile_positions=None, outfile_environment=None):
        """TODO Docstring
        """

    @abstractmethod
    def get_agents(self):
        """TODO Docstring
        """

    @abstractmethod
    def get_environment(self):
        """TODO Docstring
        """

class GridSimulation(Simulation):
    """
    Class for instancing a simulation on a 2-dimensional grid
    """
    def __init__(self, agents_set, total_timesteps, environment, agents_positions):
        super().__init__()
        self._current_time = 0
        self._total_timesteps = total_timesteps
        self._environment = environment
        self._agents_set = agents_set
        environment.add_agents(agents_set, agents_positions)
        #for agent in self._agents_set:
        #    environment.add_resources(agent.get_requirements())

    def run_simulation(self, outfile_positions=None, outfile_environment=None):
        if(outfile_positions and outfile_positions != 'STDOUT'):
            f_make_new = open(outfile_positions, 'w')
            f_make_new.close()
        if(outfile_environment and outfile_environment != 'STDOUT'):
            f_make_new = open(outfile_environment, 'w')
            f_make_new.close()
        for t_current in range(self._total_timesteps):
            self._current_time = t_current
            for agent in self._agents_set:
                # get position of agent and feed it into environment to get agent's view
                self._environment.apply_actions(agent, agent.act(
                    self._environment.get_view(agent.get_local_info())))
                if outfile_positions:
                    self._print_current_positions(outfile_positions)
                if outfile_environment:
                    self._print_current_environment(outfile_environment)
        self.end_simulation()
        return self._total_timesteps

    def end_simulation(self):
        """TODO docstring
        """
        #print("END SIMULATION: ", self._total_timesteps, " timesteps.")

    def get_agents(self):
        return self._agents_set

    def get_environment(self):
        return self._environment

    def _print_current_positions(self, outfile=None):
        """
        for each agent prints/writes to a file its position
        """
        agents = self._agents_set
        if outfile == 'STDOUT':
            for agent in agents:
                print(f"{self._environment.get_position(agent)[0]}"
                      f" {self._environment.get_position(agent)[1]}")
                print()
        else:
            with open(outfile, 'a') as f_out:
                for agent in agents:
                    f_out.write(f"{self._environment.get_position(agent)[0]}"
                                f" {self._environment.get_position(agent)[1]}\n")
                f_out.write('\n')

    def _print_current_environment(self, outfile=None):
        pass
