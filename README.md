# An ant world

As a project to learn Python best practices, I gathered some former university
colleagues with the goal of simulating an ants’ nest. Leaving the details aside (the
full project is at https://gitlab.com/gablab/an_ant_world/ ), I will focus on a couple of
technical decision that I believe improved the result.

First, the choice of a simple but powerful testing framework, Pytest, allowed back-
compatibility: every new feature had to build on the previous ones. Linting was a
warranty of readability and forced every developer to document the new code. In
order to speed up the testing, instead of relying only on the Gitlab CI, a test con-
tainer was set up with Docker. The review of merge requests has been regulated.
These core tools allowed the code to be clear and easily extended. Their setup
was made once for all.

The code is modular: creating a new type of agent (agents are the main elements
of the simulation) corresponds to the creation of just one file containing its fea-
tures and behavior; the Simulation object will take care of interfacing the agents
with the Environment object, allowing interaction.
The result is an easily mantainable code, which, besides of reaching the goal of
simulating the way ants look for food, also taught me how to use continuous in-
tegration in a real project.

