FROM       python:3.8

RUN        pip install virtualenv pylint pytest

COPY       . /ant

WORKDIR    /ant

RUN        pip install -e .

#RUN        pylint --ignored-classes=_socketobject *.py

#RUN        pytest

ENV        SHELL=/bin/bash

#ENTRYPOINT ["test", "run"]

CMD        ["pytest"]

