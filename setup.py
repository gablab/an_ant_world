#!/usr/bin/env python
"""
module of setup: run `pip install -e .`
"""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['matplotlib>=3.2.1', 'numpy>=1.18.0', 'pylint>=2.5.2', 'pytest>=5.4.2']

setup_requirements = []

test_requirements = []

setup(
    author="socioeconomicalBoh",
    author_email='SOCIO@ECONOMICAL.BOH',
    python_requires='>=3.8',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Agent based simulations",
    entry_points={
        'console_scripts': [
            'an_ant_world=an_ant_world.cli:main',
        ],
    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='an_ant_world',
    name='an_ant_world',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    setup_requires=setup_requirements,
    test_suite='unittest',
    tests_require=test_requirements,
    url='https://gitlab.com/socioeconomicalboh/an_ant_world',
    version='0.1.0',
    zip_safe=False,
)
